# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git init
git clone https://karim7557@bitbucket.org/karim7557/stroboskop.git
git add .
git commit -m "Vzpostavitev repozitorija v Cloud9"
git push origin master
```

Naloga 6.2.3:
https://bitbucket.org/karim7557/stroboskop/commits/c2f48bdda9d589b9712919740a39be3de851a8ff

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/karim7557/stroboskop/commits/70fea00820608e97fd1ed8a48828d03e1a97981b

Naloga 6.3.2:
https://bitbucket.org/karim7557/stroboskop/commits/34aff952ec4722fdd478400b80b7abdf3573401b

Naloga 6.3.3:
https://bitbucket.org/karim7557/stroboskop/commits/19915fcdc91c073bc8b79a758013f1e0539c09f5

Naloga 6.3.4:
https://bitbucket.org/karim7557/stroboskop/commits/e3293e26cb27eb1e30ab181c93e6d9eb66a67ee5

Naloga 6.3.5:

```
git checkout master
git merge izgled 
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/karim7557/stroboskop/commits/c7fb13222095ec074b9025dd118e3b990721a10b

Naloga 6.4.2:
https://bitbucket.org/karim7557/stroboskop/commits/dd41379e6bd555121dd91d74bb1593ffaaf9fb65

Naloga 6.4.3:
https://bitbucket.org/karim7557/stroboskop/commits/7c6373f513cbf590de4e0b981283f9e4dcc6d968

Naloga 6.4.4:
https://bitbucket.org/karim7557/stroboskop/commits/899d6b3bb465ca3faea253f481b908ed74fe1f12